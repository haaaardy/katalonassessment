#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@JobCategories
Feature: Title of your feature
  I want to use this template for my feature file

  @SuccessfullyAddNewJobCategory
  Scenario Outline: Successfully add new Job Category
    Given I enter <username> and <password> to login then go to Job Category page
    When I click on add button
    And I fill in a new <categoryName>
    And I click on save button
    Then I successfully add a new job category
    
  Examples: 
      | username | password			           | categoryName   |
      | Admin    | hUKwJTbofgPU9eVlw/CnDQ==| category test2 |
      
  @EnterExistedJobCategory
  Scenario Outline: User enter existing job category
    Given I enter <username> and <password> to login then go to Job Category page
    When I fill in an existed category name
    And I click on save button
    Then I failed to add a new job category
    
  Examples: 
	  | username | password			           |
	  | Admin    | hUKwJTbofgPU9eVlw/CnDQ==|
  
  
  
  
  
  
  
  	@DeleteOneJobCategory
  	Scenario Outline: Successfully delete a job category
  		Given I enter <username> and <password> to login then go to Job Category page
  		When I click on the delete button of one record
  		And I click on confirm delete button
  		Then I successfully delete the chosen record
  
	  Examples: 
			| username | password			           |
			| Admin    | hUKwJTbofgPU9eVlw/CnDQ==|
  
  
  
		@DeleteMultipleJobCategories
		Scenario Outline: Users delete more than job category
  		Given I enter <username> and <password> to login then go to Job Category page
  		When I click on the checkbox of record 1 and record 2
  		And I click on the batch delete button
  		And I click on confirm delete button
  		Then I successfully delete the chosen record
  		
		Examples: 
			| username | password			           |
			| Admin    | hUKwJTbofgPU9eVlw/CnDQ==|
  
  
  
  
  
  
  
  

