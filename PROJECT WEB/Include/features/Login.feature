#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Login
Feature: Login
  As a valid, I want to able to login in OrangeHRM system

  @Vaild
  Scenario Outline: Login in with vaild credentials
    Given I navigate to OrangeHRM Login page
    When I filled in <username> and <password>
    And I click on Login button
    Then I successfully login into OrangeHRM

    Examples: 
      | username  | password 								 |
      | Admin     | hUKwJTbofgPU9eVlw/CnDQ== |