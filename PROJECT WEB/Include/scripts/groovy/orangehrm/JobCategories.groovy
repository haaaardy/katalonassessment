package orangehrm
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.exception.StepFailedException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class JobCategories {

	@Given("I enter (.*) and (.*) to login then go to Job Category page")
	def I_enter_username_and_password_to_login_then_go_to_job_category_page(String username, String password) {
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://opensource-demo.orangehrmlive.com/web/index.php/auth/login')
		WebUI.maximizeWindow()
		WebUI.setText(findTestObject('Object Repository/Common/Page_OrangeHRM/input_Username_username'), username)
		WebUI.setEncryptedText(findTestObject('Object Repository/Common/Page_OrangeHRM/input_Password_password'), password)
		WebUI.click(findTestObject('Object Repository/Common/Page_OrangeHRM/button_Login'))
		WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/a_Admin'))
		WebUI.navigateToUrl('https://opensource-demo.orangehrmlive.com/web/index.php/admin/viewSystemUsers')
		WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/span_Job'))
		WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/a_Job Categories'))
	}

	@When("I click on add button")
	def I_click_on_add_button() {
		WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/button_Job Category Add'))
	}

	@And("I fill in a new (.*)")
	def I_fill_in_categoryname(String categoryName) {
		WebUI.setText(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/input_Job Category Name'), categoryName)
	}

	@And("I click on save button")
	def I_click_on_save_button() {
		WebUI.click(findTestObject('JobCategory/Page_OrangeHRM/button_Job Category Save'))
	}

	@Then("I successfully add a new job category")
	def I_successfully_add_a_new_job_category() {
		WebUI.verifyElementPresent(findTestObject('JobCategory/Page_OrangeHRM/div_Job Category SuccessSuccessfully Saved'), 0)
		WebUI.closeBrowser()
	}


	@When("I fill in an existed category name")
	def I_fill_in_an_existed_category_name() {
		String existedJobCategory = WebUI.getText(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/div_Job Category Name 1'))
		WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/button_Job Category Add'))
		WebUI.setText(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/input_Job Category Name'), existedJobCategory)
	}

	@Then("I failed to add a new job category")
	def I_failed_to_add_a_new_job_category() {
		WebUI.verifyElementPresent(findTestObject('JobCategory/Page_OrangeHRM/span_Job Category Already exists'), 0)
		WebUI.closeBrowser()
	}
	
	@When("I click on the delete button of one record")
	def I_click_on_the_delete_button_of_one_record() {
		WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/i_Job Category Delete Button 1'))
	}
	
	@And("I click on confirm delete button")
	def I_click_on_confirm_delete_button() {
		WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/button_Job Category Confirm Delete'))
	}
	
	@Then("I successfully delete the chosen record")
	def I_successfully_delete_the_chosen_record() {
		WebUI.verifyElementPresent(findTestObject('JobCategory/Page_OrangeHRM/div_Job Category SuccessSuccessfully Deleted'), 0)
		WebUI.closeBrowser()
	}
	
	@When("I click on the checkbox of record 1 and record 2")
	def I_click_on_the_checkbox_of_record_1_and_record_2() {
		WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/span_Delete Job Category Checkbox 1'))
		WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/span_Delete Job Category Checkbox 2'))
	}
	
	@And("I click on the batch delete button")
	def I_click_on_the_batch_delete_button() {
		WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/button_Job Category Batch Delete Selected'))
	}
	
}