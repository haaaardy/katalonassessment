package common

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import java.util.Random as Random
import com.kms.katalon.core.exception.StepFailedException as StepFailedException

import internal.GlobalVariable

public class MyUtils {

	public final String ALPHABET_IGNORE_CASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";  // 52
	public final String ALPHABET_UPPPER_CASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";  // 26
	public final String ALPHABET_LOWER_CASE = "abcdefghijklmnopqrstuvwxyz"; // 26
	public final String NUMBER = "0123456789"; // 10

	@Keyword(keywordObject = 'Utils')
	public String getRandomStr(int len) {
		Random random = new Random();

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < len; i++) {
			sb.append(ALPHABET_IGNORE_CASE.charAt(random.nextInt(ALPHABET_IGNORE_CASE.length())));
		}

		return sb.toString();
	}

	@Keyword(keywordObject = 'Utils')
	public String getRandomNumStr(int len) {
		Random random = new Random();

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < len;) {
			char ch = NUMBER.charAt(random.nextInt(NUMBER.length()));
			if (ch == '0') {
				continue;
			}
			sb.append(ch);
			i++;
		}

		return sb.toString();
	}

	@Keyword(keywordObject = 'Utils')
	public boolean verifyElementPresent(String objectId, int timeout) {
		try {
			return WebUI.verifyElementPresent(findTestObject(objectId), timeout)
		} catch (StepFailedException e) {
			return false
		}
	}

	@Keyword(keywordObject = 'Utils')
	public boolean verifyElementText(String objectId, String text, boolean regex) {
		if (this.verifyElementPresent(objectId, 2)) {
			String elementText = WebUI.getText(findTestObject(objectId))

			if (elementText == null) {
				return false;
			}

			if (regex) {
				return elementText.matches(text);
			} else {
				return elementText.equals(text);
			}
		}

		return false;
	}
}
