<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Name_oxd-input oxd-input--focus oxd-i_ad7396</name>
   <tag></tag>
   <elementGuidId>b8bc78a4-030d-4f81-b7c4-49b500c9ed76</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.oxd-input.oxd-input--focus.oxd-input--error</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[2]/div[2]/div/div/form/div/div/div[2]/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>2e857a76-08ea-406d-b510-47bf014f4fee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>oxd-input oxd-input--focus oxd-input--error</value>
      <webElementGuid>a1c183e8-7b20-41f9-8286-d8534648603e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;oxd-layout&quot;]/div[@class=&quot;oxd-layout-container&quot;]/div[@class=&quot;oxd-layout-context&quot;]/div[@class=&quot;orangehrm-background-container&quot;]/div[@class=&quot;orangehrm-card-container&quot;]/form[@class=&quot;oxd-form&quot;]/div[@class=&quot;oxd-form-row&quot;]/div[@class=&quot;oxd-input-group oxd-input-field-bottom-space&quot;]/div[2]/input[@class=&quot;oxd-input oxd-input--focus oxd-input--error&quot;]</value>
      <webElementGuid>cf9ff6a3-76fa-4b4b-b5c5-281423f180ab</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[2]/div[2]/div/div/form/div/div/div[2]/input</value>
      <webElementGuid>4c42ba55-1a6f-4997-a600-8e1991f9ffd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>2df04bca-eba6-4bb7-9eab-619063254787</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
