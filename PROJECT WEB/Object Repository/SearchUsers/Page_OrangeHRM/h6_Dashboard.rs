<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h6_Dashboard</name>
   <tag></tag>
   <elementGuidId>0f51baff-7e34-48d1-8036-7d4e33992e93</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h6.oxd-text.oxd-text--h6.oxd-topbar-header-breadcrumb-module</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/header/div/div/span/h6</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
      <webElementGuid>9594cbd5-e2df-4a97-b69e-2695e7a055c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>oxd-text oxd-text--h6 oxd-topbar-header-breadcrumb-module</value>
      <webElementGuid>28c18dee-4210-49c4-9832-a6110535e615</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Dashboard</value>
      <webElementGuid>b11005bf-3d4b-4e72-947e-c07f24d3d01b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;oxd-layout&quot;]/div[@class=&quot;oxd-layout-navigation&quot;]/header[@class=&quot;oxd-topbar&quot;]/div[@class=&quot;oxd-topbar-header&quot;]/div[@class=&quot;oxd-topbar-header-title&quot;]/span[@class=&quot;oxd-topbar-header-breadcrumb&quot;]/h6[@class=&quot;oxd-text oxd-text--h6 oxd-topbar-header-breadcrumb-module&quot;]</value>
      <webElementGuid>3f8b7acb-055e-48f8-ad5e-4a6a39983d0d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/header/div/div/span/h6</value>
      <webElementGuid>7483581f-40f8-449d-8734-6ceae22a66e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h6</value>
      <webElementGuid>270cab12-8c2d-4444-815c-7fed05bf9717</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h6[(text() = 'Dashboard' or . = 'Dashboard')]</value>
      <webElementGuid>4d585338-a599-429a-ae27-f333cf549762</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
