import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Common/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/AddEmployee/Page_OrangeHRM/span_PIM'))

WebUI.click(findTestObject('Object Repository/AddEmployee/Page_OrangeHRM/a_Add Employee'))

String firstName = CustomKeywords.'common.MyUtils.getRandomStr'(3)

String middleName = CustomKeywords.'common.MyUtils.getRandomStr'(3)

String lastName = CustomKeywords.'common.MyUtils.getRandomStr'(3)

WebUI.setText(findTestObject('Object Repository/AddEmployee/Page_OrangeHRM/input_Employee Full Name_firstName'), firstName)

WebUI.setText(findTestObject('Object Repository/AddEmployee/Page_OrangeHRM/input_Employee Full Name_middleName'), middleName)

WebUI.setText(findTestObject('Object Repository/AddEmployee/Page_OrangeHRM/input_Employee Full Name_lastName'), lastName)

WebUI.click(findTestObject('Object Repository/AddEmployee/Page_OrangeHRM/span_Create Login Details Button'))

boolean usernameOk = false

while (!(usernameOk)) {
    String username = CustomKeywords.'common.MyUtils.getRandomStr'(6)

    WebUI.setText(findTestObject('Object Repository/AddEmployee/Page_OrangeHRM/input_Employee Username'), username)

    try {
        WebUI.verifyElementPresent(findTestObject('Object Repository/AddEmployee/CaptureObject/Page_OrangeHRM/span_Username already exists'), 
            2)
    }
    catch (Exception e) {
        usernameOk = true
    } 
}

WebUI.click(findTestObject('Object Repository/AddEmployee/Page_OrangeHRM/span_Employee status Enabled'))

WebUI.setEncryptedText(findTestObject('Object Repository/AddEmployee/Page_OrangeHRM/input_Employee Password'), 'i5Tq7bplk6M3iYVeKgBYPA==')

WebUI.setEncryptedText(findTestObject('Object Repository/AddEmployee/CaptureObject/Page_OrangeHRM/input_Confirm Password'), 
    'i5Tq7bplk6M3iYVeKgBYPA==')

WebUI.verifyElementPresent(findTestObject('AddEmployee/Page_OrangeHRM/span_Password is Weak'), 0)

WebUI.click(findTestObject('Object Repository/AddEmployee/Page_OrangeHRM/button_Add Employee Save'))

WebUI.verifyElementPresent(findTestObject('AddEmployee/CaptureObject/Page_OrangeHRM/div_Employee SuccessSuccessfully Saved'), 
    0)

WebUI.closeBrowser()

