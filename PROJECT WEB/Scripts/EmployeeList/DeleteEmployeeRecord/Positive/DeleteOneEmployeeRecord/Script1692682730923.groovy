import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty

WebUI.callTestCase(findTestCase('Common/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/AddEmployee/Page_OrangeHRM/span_PIM'))

boolean hasRecords = CustomKeywords.'common.MyUtils.verifyElementText'(
				'Object Repository/AddEmployee/CaptureObject/Page_OrangeHRM/span_Employee Records Found', 
				'\\([1-9][0-9]*\\) Records Found', true);
if (hasRecords) {
	if (CustomKeywords.'common.MyUtils.verifyElementPresent'('Object Repository/AddEmployee/SearchEmployee/Page_OrangeHRM/i_Employee 1 Checkbox', 1)) {
		WebUI.click(findTestObject('Object Repository/AddEmployee/SearchEmployee/Page_OrangeHRM/i_Delete Employee 1 Button'))
	} else {
		WebUI.click(findTestObject('Object Repository/AddEmployee/SearchEmployee/Page_OrangeHRM/i_Delete Employee 2 Button'))
	}
	
	WebUI.click(findTestObject('Object Repository/DeleteEmployee/Page_OrangeHRM/button_Employee Confirm Delete'))
	WebUI.verifyElementPresent(findTestObject('Object Repository/DeleteEmployee/Page_OrangeHRM/div_Employee SuccessSuccessfully Deleted'), 0)
}

WebUI.closeBrowser()
