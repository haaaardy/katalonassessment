import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Common/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/a_Admin'))

WebUI.navigateToUrl('https://opensource-demo.orangehrmlive.com/web/index.php/admin/viewSystemUsers')

WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/span_Job'))

WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/a_Job Categories'))

boolean hasRecords = CustomKeywords.'common.MyUtils.verifyElementText'(
	'Object Repository/JobCategory/Page_OrangeHRM/span_Job Category Records Found',
	'\\([1-9][0-9]*\\) Records Found', true);

if (hasRecords) {
    WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/span_Delete Job Category Checkbox 1'))
	
	if (CustomKeywords.'common.MyUtils.verifyElementPresent'('Object Repository/JobCategory/Page_OrangeHRM/span_Delete Job Category Checkbox 2', 2)) {
		WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/span_Delete Job Category Checkbox 2'))
	}
	
	WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/button_Job Category Batch Delete Selected'))
    WebUI.click(findTestObject('Object Repository/JobCategory/Page_OrangeHRM/button_Job Category Confirm Delete'))

    WebUI.verifyElementPresent(findTestObject('JobCategory/Page_OrangeHRM/div_Job Category SuccessSuccessfully Deleted'), 
        0)
}

WebUI.closeBrowser()

