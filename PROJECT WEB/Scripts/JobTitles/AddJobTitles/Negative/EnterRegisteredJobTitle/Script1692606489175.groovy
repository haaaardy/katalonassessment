import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Common/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/JobTitles/Page_OrangeHRM/a_Admin'))

WebUI.click(findTestObject('Object Repository/JobTitles/Page_OrangeHRM/span_Job'))

WebUI.click(findTestObject('Object Repository/JobTitles/Page_OrangeHRM/a_Job Titles'))


boolean hasRecords = CustomKeywords.'common.MyUtils.verifyElementText'(
	'Object Repository/JobTitles/Page_OrangeHRM/span_Job Titles Records Found',
	'\\([1-9][0-9]*\\) Records Found', true);


if (hasRecords) {
	// Get existed Job Title
	String existedJobTitle = WebUI.getText(findTestObject('Object Repository/JobTitles/Page_OrangeHRM/div_Job Title 1'))
	
	WebUI.click(findTestObject('Object Repository/JobTitles/Page_OrangeHRM/button_Add'))
	WebUI.setText(findTestObject('Object Repository/JobTitles/Page_OrangeHRM/input_Job Title'), existedJobTitle)
	
	WebUI.setText(findTestObject('Object Repository/JobTitles/Page_OrangeHRM/textarea_Job Description'), 'Test description')
	
	WebUI.setText(findTestObject('Object Repository/JobTitles/Page_OrangeHRM/textarea_Note'), 'Test Note')
	
	WebUI.uploadFile(findTestObject('Object Repository/JobTitles/Page_OrangeHRM/i_Job Title Upload File Button'), 'C:\\workspace\\project\\automatedTesting\\Assessment\\PROJECT WEB\\Test File.xlsx')
	
	WebUI.verifyTextNotPresent('No file chosen', false)
	
	WebUI.click(findTestObject('JobTitles/Page_OrangeHRM/button_Save'))
	
	WebUI.verifyElementPresent(findTestObject('JobTitles/Page_OrangeHRM/span_Job Title Already exists'), 0)
	
	WebUI.verifyElementText(findTestObject('JobTitles/Page_OrangeHRM/span_Job Title Already exists'), 'Already exists')
}



WebUI.closeBrowser()

