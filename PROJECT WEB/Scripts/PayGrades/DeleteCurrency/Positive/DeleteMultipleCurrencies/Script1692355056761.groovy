import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty

WebUI.callTestCase(findTestCase('Common/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/a_Admin'))

WebUI.navigateToUrl('https://opensource-demo.orangehrmlive.com/web/index.php/admin/viewSystemUsers')

WebUI.click(findTestObject('Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/span_Job'))

WebUI.click(findTestObject('Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/a_Pay Grades'))

boolean hasPayGradeRecords = CustomKeywords.'common.MyUtils.verifyElementText'(
			'Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/span_Pay Grade Records Found',
			'\\([1-9][0-9]*\\) Records Found', true);
if (hasPayGradeRecords) {
        WebUI.click(findTestObject('Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/i_Pay Grade 2 Edit'))

        // Add Currency 1
        WebUI.click(findTestObject('PayGrades/Page_OrangeHRM/Page_OrangeHRM/button_Add Currency'))

        WebUI.click(findTestObject('PayGrades/Page_OrangeHRM/i_-- Select Currency'))

        WebUI.click(findTestObject('PayGrades/Page_OrangeHRM/div_CNY - Chinese Yuan Renminbi'))

        WebUI.setText(findTestObject('PayGrades/Page_OrangeHRM/Page_OrangeHRM/input_New Currency Minimum Salary'), '1000')

        WebUI.setText(findTestObject('PayGrades/Page_OrangeHRM/Page_OrangeHRM/input_New Currency Maximum Salary'), '2000')

        WebUI.click(findTestObject('PayGrades/Page_OrangeHRM/button_New Currency Save'))

        // Add Currency 2
        WebUI.click(findTestObject('PayGrades/Page_OrangeHRM/Page_OrangeHRM/button_Add Currency'))

        WebUI.click(findTestObject('PayGrades/Page_OrangeHRM/i_-- Select Currency'))

        WebUI.click(findTestObject('PayGrades/Page_OrangeHRM/div_Unkown Currency After CNY'))

        WebUI.setText(findTestObject('PayGrades/Page_OrangeHRM/Page_OrangeHRM/input_New Currency Minimum Salary'), '1000')

        WebUI.setText(findTestObject('PayGrades/Page_OrangeHRM/Page_OrangeHRM/input_New Currency Maximum Salary'), '2000')

        WebUI.click(findTestObject('PayGrades/Page_OrangeHRM/button_New Currency Save'))
		
		// Reaccess Pay Grade
        WebUI.click(findTestObject('PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/span_Job'))
        WebUI.click(findTestObject('PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/a_Pay Grades'))
		WebUI.click(findTestObject('Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/i_Pay Grade 2 Edit'))
        
        boolean hasCurrencyRecords = CustomKeywords.'common.MyUtils.verifyElementText'(
						'Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/span_Currency Records Found',
						'\\([1-9][0-9]*\\) Records Found', true);

        if (hasCurrencyRecords) {
			if (CustomKeywords.'common.MyUtils.verifyElementPresent'('Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/i_Currency 1', 2)) {
				WebUI.click(findTestObject('Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/i_Currency 1'))
			}
			if (CustomKeywords.'common.MyUtils.verifyElementPresent'('Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/i_Currency 2', 2)) {
				WebUI.click(findTestObject('Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/i_Currency 2'))
			}
			
			WebUI.click(findTestObject('Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/button_Currency Delete Selected'))
			WebUI.click(findTestObject('Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/button_Currency Confirm Delete'))
			WebUI.verifyElementPresent(findTestObject('Object Repository/PayGrades/Page_OrangeHRM/DeleteOneCurrency/Page_OrangeHRM/div_Currency Successfully Deleted'), 0)
        }
}

WebUI.closeBrowser()

