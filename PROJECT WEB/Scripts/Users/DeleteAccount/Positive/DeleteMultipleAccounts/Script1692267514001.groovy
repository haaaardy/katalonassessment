import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty

WebUI.callTestCase(findTestCase('Common/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/DeleteAccount/Page_OrangeHRM/a_Admin'))

boolean hasRecords = CustomKeywords.'common.MyUtils.verifyElementText'(
	'Object Repository/DeleteAccount/Page_OrangeHRM/span_User Records Found',
	'\\([1-9][0-9]*\\) Records Found', true);
if (hasRecords) {
	if (CustomKeywords.'common.MyUtils.verifyElementPresent'('Object Repository/DeleteAccount/Page_OrangeHRM/i_User 1 Checkbox', 2)) {
		WebUI.click(findTestObject('Object Repository/DeleteAccount/Page_OrangeHRM/i_User 1 Checkbox'))
	}
	if (CustomKeywords.'common.MyUtils.verifyElementPresent'('Object Repository/DeleteAccount/Page_OrangeHRM/i_User 2 Checkbox', 2)) {
		WebUI.click(findTestObject('Object Repository/DeleteAccount/Page_OrangeHRM/i_User 2 Checkbox'))
	}
	if (CustomKeywords.'common.MyUtils.verifyElementPresent'('Object Repository/DeleteAccount/Page_OrangeHRM/i_User 3 Checkbox', 2)) {
		WebUI.click(findTestObject('Object Repository/DeleteAccount/Page_OrangeHRM/i_User 3 Checkbox'))
	}
	
	if (CustomKeywords.'common.MyUtils.verifyElementPresent'('Object Repository/DeleteAccount/Page_OrangeHRM/button_Delete Selected', 2)) {
		WebUI.click(findTestObject('Object Repository/DeleteAccount/Page_OrangeHRM/button_Delete Selected'))
		WebUI.click(findTestObject('Object Repository/DeleteAccount/Page_OrangeHRM/button_Yes, Delete'))
		WebUI.verifyElementPresent(findTestObject('Object Repository/DeleteAccount/Page_OrangeHRM/div_SuccessSuccessfully Deleted'), 0)
	}
}

WebUI.closeBrowser()

